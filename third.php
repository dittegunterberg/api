<form action="third.php" method="get">
    <input name="id" type="number">
    <input type="submit" value="Sök id"/>
</form>

<?php
require('config.php');

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $stm_select = $pdo->prepare("SELECT miletech.*, miletech_address.id AS adress_id, miletech_address.customer_id,
    miletech_address.customer_address_id, miletech_address.email AS address_email, miletech_address.firstname AS address_firstname, 
    miletech_address.lastname AS address_lastname, miletech_address.postcode, miletech_address.street, miletech_address.city, 
    miletech_address.telephone, miletech_address.country_id, miletech_address.address_type, miletech_address.company, miletech_address.country 
    FROM miletech LEFT JOIN miletech_address ON miletech_address.customer_id = miletech.id 
    WHERE miletech.id = $id");

    $stm_select->execute();
    $result = $stm_select->fetchAll(PDO::FETCH_ASSOC);
    $json = json_encode($result, JSON_PRETTY_PRINT);


    if ($json == "[]") {
        header("HTTP/1.0 404 Not Found");
        echo "<h1>404</h1><h2>Den här användaren finns inte.</h2>";
    } else {
        echo $json;
    }
}