<?php
require('config.php');

$sql = "SELECT * FROM `miletech`";
$stm = $pdo->prepare($sql);
$stm->execute([]);
$data = $stm->fetchAll();

$companies = [];

foreach ($data as $customer) {
  $companies[] = $customer['customer_company'];
}

$companies = array_unique($companies);

foreach ($companies as $company) {
 $sql_insert = "INSERT INTO `companies` (`company_name`) VALUES (:company_name)";
  $stm_insert = $pdo->prepare($sql_insert);
  $stm_insert->execute([
    ':company_name' => $company
  ]);
}

$sql_select = "SELECT * FROM `companies`";
$stm_select = $pdo->prepare($sql_select);
$stm_select->execute([]);
$data = $stm_select->fetchAll();

foreach ($data as $company_id) {
  $sql_update = "UPDATE `miletech` SET `company_id` = :company_id WHERE customer_company = :customer_company";
  $stm_update = $pdo->prepare($sql_update);
  $stm_update->execute([
    ':company_id' => $company_id['id'],
    ':customer_company' => $company_id['company_name']
  ]);
}